package com.peterwkc.Listener;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.peterwkc.Manager.WebDriverManager;

import io.qameta.allure.Attachment;

public class TestListener implements ITestListener {
  
	private WebDriverManager manager;
	
	public TestListener() {
		manager = WebDriverManager.createDriver();
	}
	
	private String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }
	
	 //Text attachments for Allure
    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshotPNG (WebDriver driver) {
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }

    //Text attachments for Allure
    @Attachment(value = "{0}", type = "text/plain")
    public static String saveTextLog (String message) {
        return message;
    }

    //HTML attachments for Allure
    @Attachment(value = "{0}", type = "text/html")
    public static String attachHtml(String html) {
        return html;
    }

	@Override
	public void onTestStart(ITestResult result) {
		System.out.println("Test Start: " +  getTestMethodName(result));    
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println("Test Success: " +  getTestMethodName(result));    
	}

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("Test Failure: " +  getTestMethodName(result));  
	
		//Get driver from BaseTest and assign to local webdriver variable.
        Object testClass = result.getInstance();
        WebDriver driver = manager.getDriver();
        
        //Allure ScreenShotRobot and SaveTestLog
        if (driver instanceof WebDriver) {
            System.out.println("Screenshot captured for test case:" + getTestMethodName(result));
            saveScreenshotPNG(driver);
        }

        //Save a log on allure.
        saveTextLog(getTestMethodName(result) + " failed and screenshot taken!");
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		System.out.println("Test Skipped: " +  getTestMethodName(result)); 
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		System.out.println("Test Failed within Success Percentage: " +  getTestMethodName(result)); 
		
	}

	@Override
	public void onStart(ITestContext context) {
		System.out.println("On Start: " +  context.getName()); 
	}

	@Override
	public void onFinish(ITestContext context) {
		System.out.println("On Finish: " +  context.getName()); 
	}

}
