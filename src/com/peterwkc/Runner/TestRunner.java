package com.peterwkc.Runner;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.peterwkc.Listener.TestListener;
import com.peterwkc.Manager.WebDriverManager;

import io.qameta.allure.*;

@Listeners({TestListener.class})
@Feature("Test Runner Tests")
public class TestRunner {
	private WebDriverManager manager;
	
	public TestRunner() {
	}
	
	@BeforeSuite
	public void setup() {
		manager = WebDriverManager.createDriver();
	}

	@Test
	@Description("Google search test.")
	public void googleSearch() {
		manager.getDriver().navigate().to("http://www.google.com.my");
	}
	
	
	@AfterSuite
	public void teardown() {
		WebDriverManager.tearDown();
	}

}

	/*
	 * After run the testng tests, generate allure report from jnit test report xml file using
	 * command 
	 * allure serve /target/allure-results/*.xml
	 * 
	 * Epic > Features > Stories
	 * 
	 */















