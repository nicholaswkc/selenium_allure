package com.peterwkc.Manager;

import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverManager {

	public static WebDriver driver = null;
	private static WebDriverManager manager = null;
	private WebDriverManager() {
	}
		
	public static WebDriverManager createDriver() {
		try {
			if (driver == null) {
				System.setProperty("webdriver.chrome.driver","C:\\Users\\peter\\chromedriver.exe");
				driver = new ChromeDriver();
				manager = new WebDriverManager();
			}
		} catch (Exception ex) {
			LogManager.logger.log(Level.INFO, "Exception: " + ex.getMessage());
		}
		
		return manager;

	}
	
	public WebDriver getDriver() {
		return driver;
	}
	
	public static void tearDown() {
		try {
			driver.close();
			driver.quit();
		} catch (Exception ex) {
			LogManager.logger.log(Level.INFO, "Exception: " + ex.getMessage());
		}
	}
	
	

}
